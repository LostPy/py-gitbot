CREATE TABLE Guild (
	idGuild INT PRIMARY KEY AUTO_INCREMENT,
	name VARCHAR(100) NOT NULL,
    language: VARCHAR(2) DEFAULT "en",
);

CREATE TABLE Sheet (
	idSheet INT PRIMARY KEY AUTO_INCREMENT,
    title VARCHAR(20) NOT NULL,
    state TINYINT DEFAULT 2,
    category VARCHAR(30) NOT NULL,
    rate TINYINT DEFAULT 1,
    description VARCHAR(150),
    guild INT NULL,
    
    FOREIGN KEY (guild) REFERENCES Guild(idGuild)
);

/* For a Game system:

CREATE TABLE Ranking (
	idRanking INT PRIMARY KEY AUTO_INCREMENT,
    guild INT NOT NULL,
    user INT NOT NULL,
    score INT DEFAULT 0, // total score
    total INT DEFAULT 0, // number of questions played
    success INT DEFAULT 0 // number of questions passed with success
);
*/

