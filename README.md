# gitbot

A Discord Bot to learn Git commands on Discord.

## Overview

 * Author: LostPy
 * Date: 2022-05-02
 * License: GPL 3.0
 * Python 3.9

## License

```
GitBot is a Discord bot to learn Git commands on Discord.
Copyright (C) 2022  LostPy

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```